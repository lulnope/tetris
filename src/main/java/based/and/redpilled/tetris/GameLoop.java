package based.and.redpilled.tetris;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class GameLoop extends AnimationTimer {

	private static final Double NANOS_IN_SECOND = 1_000_000_000d;

	@NonNull
	private final GraphicsContext gc;

	private final long start = System.nanoTime();
	private long previousFrameTime = System.nanoTime();

	private final Image blue = new Image("assets/tetris_block_blue_32.bmp");

	private double rotation = 0;

	@Override
	public void handle(long now) {
		double totalDeltaT = (now - start) / NANOS_IN_SECOND;
		double frameDeltaT = (now - previousFrameTime) / NANOS_IN_SECOND;
		previousFrameTime = System.nanoTime();

		final double width = gc.getCanvas().getWidth();
		final double height = gc.getCanvas().getHeight();

		gc.setFill(Color.RED);
		gc.fillRect(0, 0, width, height);

		int radius = 100;
		double x = width / 2 + radius * Math.cos(totalDeltaT) - blue.getWidth() / 2;
		double y = height / 2 + radius * Math.sin(totalDeltaT) - blue.getHeight() / 2;

		double degreesPerSecond = 360;

		rotation = rotation + frameDeltaT * degreesPerSecond;

		gc.save();
		final Rotate r = new Rotate(rotation, x + blue.getWidth() / 2, y + blue.getHeight() / 2);
		gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
		gc.drawImage(blue, x, y);
		gc.restore();
	}
}
