package based.and.redpilled.tetris;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

import java.io.IOException;

public class TetrisApplication extends Application {

	@Override
	public void start(Stage stage) throws IOException {
		int width = 800;
		int height = 600;

		Canvas canvas = new Canvas(width, height);
		final GraphicsContext gc = canvas.getGraphicsContext2D();
		final GameLoop gameLoop = new GameLoop(gc);
		gameLoop.start();

		Group root = new Group();
		root.getChildren().add(canvas);

		Scene scene = new Scene(root, width, height);
		stage.setTitle("Based And Redpilled Tetris");
		stage.setScene(scene);
		stage.setResizable(false);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}
}