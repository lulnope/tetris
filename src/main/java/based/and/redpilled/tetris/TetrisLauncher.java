package based.and.redpilled.tetris;

public class TetrisLauncher {

	public static void main(String[] args) {
		// this class is a hack to circumvent JavaFx11+ JPMS enforcement: https://mail.openjdk.org/pipermail/openjfx-dev/2020-April/025903.html
		TetrisApplication.main(args);
	}
}
